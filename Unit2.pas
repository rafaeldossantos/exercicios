unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Unit3,
  System.Net.URLClient, System.Net.HttpClient, System.Net.HttpClientComponent,
  Vcl.Menus;

type
  TForm2 = class(TForm)
    Button1: TButton;
    MainMenu1: TMainMenu;
    Arquivo1: TMenuItem;
    Ajuda1: TMenuItem;
    Ajuda2: TMenuItem;
    Download1: TMenuItem;
    NetHTTPClient1: TNetHTTPClient;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Download1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
begin
  Form3 := TForm3.Create(Application);
  Form3.Show;
end;

procedure TForm2.Download1Click(Sender: TObject);
begin
  Memo1.Text:= NetHTTPClient1.Get('https://venson.net.br/').ContentAsString();
end;

end.
